# Coding-Weeks Project: Web, DL and Audio

In this project, we'll go through some basic concepts in web dev, deep learning and audio manipulation, the main parts are:

<p align="center"><img src="images/webpage.png" align="center" width="750"></p>
<p align="center"><img src="images/todo.png" align="center" width="600"></p>
<p align="center"><img src="images/additem.png" align="center" width="600"></p>

### Part 1: [Project's webpage with Flask](flask.md)
We'll create a simple website for your coding weeks project. It'll have some information about your team, the projet's idea and a To-Do list page with the project's tasks to be accomplished.

### Part 2: [Adding a to MySQL database](database.md)
We'll extend our website with some additionnal functionnality. Such as using a MySQL database to store the To-Do list, and add the possibility of adding, editing and deleting the items of the To-Do list from within our website.

### Part 3: [Creating a REST API](REST.md)
As a continuation of the first two parts, we'll create a simple REST API to present 3rd party users the possibility to access our To-Do list via HTTP requests.

### Part 4: [Intro Deep learning with Keras](intro_keras.md)
We'll go to the second focus of this project, which is Deep Learning. We'll start with a simple introduction to some basic Machine and Deep learning concepts using Keras.


### Part 5: [Trigger words detection](trigger_word_detection/README.md)
As a practical application of Deep Learning. We'll implement a trigger word detector. Where each time we detect the corresponding sound to a given word tigger word, we need to detect it, and output a "bell" sound.

### Part 6: [Putting it together: Keras, flask and REST](keras_REST.md)
In this final part, we'll try to re-use all of the concepts we've learned thus far. And create a REST API to access our trigger word detector via the web.

## Resources

The tutorials in this Coding-Weeks project were based on the following:

- [Flask Web Development](https://flaskbook.com/#) book by Miguel Grinberg.
- [Kaggle competition](https://www.kaggle.com/c/tensorflow-speech-recognition-challenge)
- [DL course](https://www.deeplearning.ai/).
- Sentdex [Flask](https://www.youtube.com/playlist?list=PLQVvvaa0QuDc_owjTbIY4rbgXOFkUYOUB) and [DL](https://www.youtube.com/playlist?list=PLQVvvaa0QuDdeMyHEYc0gxFpYwHY2Qfdh) tutorials.
- [Keras REST API tutorial](https://blog.keras.io/building-a-simple-keras-deep-learning-rest-api.html).


### Required packages

The required packages that we'll be using during coding weeks can be viewd in `requirements.txt`. You can install all of them in one go if you want with `pip install -r requirements.txt`, but make sure you're using a `virualenv`. Better to go with the flow of the tutorials. Each time we introduce a new package during the week, you'll be instructed to install it.

