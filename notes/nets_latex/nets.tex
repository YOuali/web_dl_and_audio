\documentclass[a4paper]{article}
\usepackage[top=1in, bottom=1in, left=1in, right=1in]{geometry}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage[export]{adjustbox}
\usepackage{float}
\usepackage{hyperref}
\usepackage{amsfonts}
\usepackage{fancyvrb}
\usepackage{etoc}
\usepackage{minitoc}


\title{ \LARGE \textbf{Logistic Regression \&  Neural Networks} \\ \large Notes for Coding-Weeks}
\author{\textbf{CentraleSupélec}}
\date{}
\setcounter{tocdepth}{3}
\setcounter{secttocdepth}{3}


\begin{document}

\maketitle


\section*{Logistic Regression}

In Logistic Regression, the objective is to predicted a target value $y_{p}$ (e.g. is the input an image of a circle or square). To predict the correct values, we'll use a dataset of pairs (input $X$, target $y$), e.g. a set of images, each image with its corresponding labels (a square or a circle). A dataset is generally divided into two parts, a training set used to find a good model and a test set to make sure our model works. We'll use the training to automatically learn a set of weights and biases ($W, b$), and use these weights to predict $y_{p}$ for new and unseen images, where $y_{p} = (W^T  X + b)$. Where $(W, b)$ are the parameters of our Logistic regression, denoted as $\theta $. If we want the outputs to be in the range of [0, 1], we can pass them through a logistic function: $\frac{1}{1+\exp (-y_{p})} = \sigma(y_{p})$\\


Logistic regression is a supervised (i.e. we have a dataset of pairs input $X$, target $y$ as a starting point) classification algorithm. In a classification problem, the target variable (or output) $y$, can take only discrete values for a given set of features (or inputs) $X$.
We can also say that the target variable is categorical. Based on the number of categories, Logistic regression can be classified as:

\begin{enumerate}
\item binomial / binary: target variable can have only 2 possible types: “0” or “1” which may represent “win” vs “loss”, “pass” vs “fail”, “dead” vs “alive”, etc.
\item multinomial: target variable can have 3 or more possible types which are not ordered(i.e. types have no quantitative significance) like “disease A” vs “disease B” vs “disease C”.
\item ordinal: it deals with target variables with ordered categories. For example, a test score can be categorized as: “very poor”, “poor”, “good”, “very good”. Here, each category can be given a score like 0, 1, 2, 3. \newline
\end{enumerate}


In a binary classification problem (with classes 1 and 0),  we use the inputs $X$ to arrive at a prediction $y_{p}$ where, depending on the value of the output we classify that observation as class 1 or class 0. Let's say our output $y_{p}$ is in the range of [0, 1]. if $y_{p} \geq 0.5$ we predict that the input $X$ belongs to class $1$, otherwise we predict class 0.\\


Now, how do we judge the performance of our Logistic Regression? ideally, we want to maximize the accuracy, by giving a zero loss if sign of $y_{p}$ agree with the sign of the true class, i.e. $y. y_{p} \geq 0$ . Otherwise, if we make the wrong prediction, the loss is 1.\\

So the objective is to always maximize $P(y|x)$. When the true class is 1, we'd like $y_{p}$ to be as close to 1 as possible to maximize $P(y|x)$. Otherwise, with a true class of 0, we'd like $y_{p}$ minimal to predict the correct class 0. This can be formulated as follows:

\begin{itemize}
\item If $y = 1$ , $P(y|x) = y_{p}$.
\item If $y = 0$, $P(y|x) = 1 - y_{p}$.
\end{itemize}

We can merge the two conditions as follows : $P(y|x) = y_{p}^y \times (1- y_{p})^{(1 - y)}$. The objective is to maximize $P(y|x)$, given that for multiple examples, we'd like to maximize the product of their probabilities, so it is easier to use the log, which is a monotonic function, and maximize their summation, or minimize the loss.\\

The loss function, that is the closer to such objective, while being a convex function is:

$$L(y, y_{p}) = - y\  \log(y_{p}) - (1 - y))  \log(1 - y_{p})$$

If $y = 0$, so we want $y_{p}$ to be small, and this is done by minimizing the loss $L = - \log(1 - y_{p})$.\\

We'd like to have a good accuracy across the whole dataset. So to have a cost function that measures the performances over all the training set, we can just average loss over all examples $i \in [1, m]$.

$$ J = \frac{1}{m} \sum_{i=1}^m L(y^i, y_{p}^i)$$.\\

We now have a cost function that measures how well the current parameters $\theta = (W, b)$ fit our training data. We can learn to classify our training data by minimizing $J$ to find the best choice of ($W, b$). Once we have done so, we can classify a new test point as class 1 or class 0 by checking which of these two class labels is most probable: if $P(y=1 | x)>P(y=0 | x)$ then we label the example as a class 1, and “0” otherwise. This is the same as checking whether $y_{p} \geq 0.5$.\\

To minimize $J$ we need to provide a function that computes $J$ and $\nabla_{\theta} J$ for any requested choice of $\theta$. We can the use the gradients to update the parameters of the logistic regressions in an iterative manner to minimize the cost functions.


\subsection*{Logistic regression as a neural network}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\linewidth]{../../images/NN.png}
\end{figure}

Logistic regression can also be viewed as a one layer neural net, let's go through the steps of computation. We have two main steps, the forward pass where we compute a prediction for a given input, and the backward pass, where we use the labels to calculate the loss, the derivatives of the loss with respect to the parameters and we then update the parameters. \\

Using only one training pair ($x^{(i)}, y^{(i)}$). We'll denote the unbounded output (i.e. before applying the sigmoid function to have a probability) as $z^{(i)}$. The forward pass is conducted as follows:

\begin{itemize}
\item $z^{(i)} = w^T x^{(i)} + b$.
\item $y_{p}^{(i)} = a^{(i)} = \sigma(z^{(i)})$.
\item $L(y_{p}^{(i)}, y^{(i)}) =  - y^{(i)}  \log(y_{p}^{(i)}) - (1-y^{(i)} )  \log(1-y_{p}^{(i)})$.\newline
\end{itemize}

In a vectorized form:
\begin{itemize}
\item Get an training set X (m examples)
\item Compute $Y_p = \sigma(W^T X + b)$
\item Calculate the cost function: $J = -\frac{1}{m}\sum_{i=1}^{m}L(y_{p}^{(i)}, y^{(i)})$. \newline
\end{itemize}

Now that we have the loss, we can apply the backward pass and update our parameters $\theta$. This process is called Gradient Descent:
\begin{itemize}
\item Compute gradients of the cost function with respect to the weights $W$: $\frac{\partial J}{\partial w} = \frac{1}{m}X(A-Y)^T$
\item And to the bias $\frac{\partial J}{\partial b} = \frac{1}{m} \sum_{i=1}^m (a^{(i)}-y^{(i)})$
\item For a parameter $\theta$ (weights \& bias), we update by $\theta = \theta - \alpha \text{ } d\theta$, where $\alpha$ is the learning rate, 
\end{itemize}

In order for Gradient Descent to work we must choose the learning rate wisely. The learning rate $\alpha$ determines how rapidly we update the parameters. If the learning rate is too large we may "overshoot" the optimal value. Similarly, if it is too small we will need too many iterations to converge to the best values. That's why it is crucial to use a well-tuned learning rate. 



\section*{Deeper Neural Nets} 
Now instead of having only one hidden layer, we can add as many as we want to give the network the ability to computes more complex functions, and the same computations we've seen above are applied.\\

Notations :
\begin{itemize}
\item Super-script $[l]$ denotes a quantity associated with the $l^{th}$ layer. \\
    - Example: $a^{[L]}$ is the $L^{th}$ layer activation. $W^{[L]}$ and $b^{[L]}$ are the $L^{th}$ layer parameters.
\item Super-script $(i)$ denotes a quantity associated with the $i^{th}$ example. \\
    - Example: $x^{(i)}$ is the $i^{th}$ training example.
\item  Lower-script $i$ denotes the $i^{th}$ entry of a vector. \\
    - Example: $a^{[l]}_i$ denotes the $i^{th}$ entry of the $l^{th}$ layer's activations)
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{../../images/deeperNN.png}
\end{figure}

\paragraph{Forward Pass}

In forward propagation, we apply the same operation $Z^{[l]} = W^{[l]}A^{[l-1]} +b^{[l]}$ to go from one layer $(l - 1)$ to the next $(l)$ using the weight matrix $W^{[l]}$ and bias $b^{[l]}$.

In general the operations applied are as follows. (LINEAR $\rightarrow$ ACTIVATION) $\times$ ($L-1$ times) $\rightarrow$ LINEAR $\rightarrow$ SIGMOID.
\begin{itemize}
\item LINEAR : $Z^{[l]} = W^{[l]}A^{[l-1]} +b^{[l]}$.
\item ACTIVATION : either $A^{[l-1]} = Relu(Z^{[l]})$ or $A^{[l-1]} = \tanh(Z^{[l]})$.
\item SIGMOID : $a^{[L]} = y_{p} = \sigma(Z^{[L]})$.
\end{itemize}

Similar to Logistic Regression, we compute the cost function $J$ across the whole training set : 

$$J = -\frac{1}{m} \sum\limits_{i = 1}^{m} (y^{(i)}\log\left(a^{[L] (i)}\right) + (1-y^{(i)})\log\left(1- a^{[L](i)}\right)$$.

\paragraph{Backward Pass}

In the backward pass, also called back-propagation, the goal is to calculate the derivatives of the cost function with respect to the parameters of the model (the weights and biases), to be able to update them using gradient descent.\\

The back-propagation is very similar to the forward propagation, the difference is that this time instead of calculating the activations for the first layer to the last, we'll back-propagate the error from the last layer to the first, each time step, by calculating the derivatives of the cost with respect to the activations $\frac{\partial{J}}{\partial{a^l}}$. and reuse $\frac{\partial{J}}{\partial{a^l}}$ of the last layer to calculate the derivatives of the cost with respect to the weights and biases.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{../../images/backwardpass.png}
\end{figure}










\end{document}
              
            