\documentclass[a4paper]{article}
\usepackage[top=1in, bottom=1in, left=1in, right=1in]{geometry}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage[export]{adjustbox}
\usepackage{float}
\usepackage{hyperref}
\usepackage{amsfonts}
\usepackage{fancyvrb}
\usepackage{etoc}
\usepackage{minitoc}
\def\code#1{\texttt{#1}}

\title{ \LARGE \textbf{Deep Learning Methods} \\ \large Notes for Coding-Weeks}
\author{\textbf{CentraleSupélec}}
\date{}
\setcounter{tocdepth}{3}
\setcounter{secttocdepth}{3}


\begin{document}

\maketitle

Now we'll will go through some basic techniques of getting deep learning to work well.

\subsection*{Normalization}

We can normalize our example (inputs) by subtracting the mean and scaling but the variance. But we must apply the same mean and variance to the test set too and not limit ourselves to the training set. This helps the gradient descent to find the optima much quickly with less iterative steps.

\subsection*{Initialization}

\paragraph{Zero initialization}

In general, initializing all the weights to zero results in the network failing to break symmetry. This means that every neuron in each layer will learn the same thing, so we might as well be training a neural network with a single neuron for every layer, and the network is no more powerful than a linear classifier such as logistic regression. It is however okay to initialize the biases $b^{[l]}$ to zeros. Symmetry is still broken so long as the weights $W^{[l]}$ are initialized randomly.

\paragraph{Random initialization}


If we start with a random initialization of the weights. The cost starts very high. This is because with large random-valued weights, the last activation (sigmoid) outputs results that are very close to 0 or 1 for some examples, and when it gets that example wrong it incurs a very high loss for that example. Indeed, when $\log(\hat{y}) = \log(0)$, the loss goes to infinity.\\

Note that:
\begin{itemize}
\item Poor initialization can lead to vanishing/exploding gradients, which also slows down the optimization algorithm.
\item If we train the network longer we will see better results, but initializing with overly large random numbers slows down the optimization.
\end{itemize}

\paragraph{A better initialization}

Given that we'd like to have the activations Z with zero mean and unit variance, for that, it is better to initialize the weights of the network from a Gaussian distribution and multiply by $\frac{1}{N(l-1)}$ to ensure unit variance in case of sigmoid non linearities, $\frac{2}{N(l-1)}$  works better for ReLU (i.e. $\text{ReLU}(x) = \max(0, x)$), for $tanh$ we use $sqrt(\frac{1}{N(l-1)})$, other version is $sqrt(\frac{1}{N(l-1) + N(l)})$.

\paragraph{As a summary}
\begin{itemize}
\item Initializing weights to very large random values does not work well. 
\item Hopefully initializing with small random values does better and it is used to get the variance equal to one. The important question is: how small should be these random values be? We have different possibilities:\\
    - Xavier initialization: uses a scaling factor for the weights $W^{[l]}$ of \code{sqrt(1.\/layers\_dims[l-1])}.\\
    - He initialization uses \code{sqrt(2.\/layers\_dims[l-1])}. Works well for networks with ReLU activations.
\end{itemize}

\subsection*{Variance \& Bias}
In a nutshell, High variance = over-fitting, the model memorizes the training data, High bias = under-fitting, the model is having problems learning from the data.\\

If the error in the train set and test set are both high, then we have both high bias and high variance. This case is very possible in high dimensions, where we have high bias in some regions and high variance in some other regions.

To get rid of high bias, we can train a bigger network until the bias is reduced, if we have high variance, we can try getting more data, applying some regularization, or trying new and more restrictive architectures.


\subsection*{Regularization}

\paragraph{L1 \& L2}

Using $L1$ regularization will make the network's weights $W$ sparse. And what that means that the matrix $W$ will have a lot of zeros in it. This can help with compressing the model, because a good number of parameters are zero, and we need less memory to store the model. But in practice this does not help that much,

\begin{itemize}
\item $L2$: we add the normalized sum of the square weights to the loss.\\ $$\frac{\lambda}{2 m} \|\omega\|_{2}^{2}= \frac{\lambda}{2 m} \sum_{j=1}^{n_{x}} \omega_{j}^{2}$$
\item $L1$: we add the normalized sum of the absolute value of weights to the loss.\\
$$\frac{\lambda}{2 m}\|w\| = \frac{\lambda}{2 m} \sum_{j=1}^{n_{x}}\left|w_{j}\right|$$
\end{itemize}

Both with a hyper-parameter $\lambda$ to  control how much regularization we apply to the weights of the model.\\

For Neural network, L2 regularization is the Frobenius Norm, also called weight decay (weight decay is a regularization technique such as L2 regularization) that results in gradient descent shrinking the weights on every iteration).\\

Regularization helps us train a simpler network, and for $tanh$ per example, it helps us stay in the linear region (a simpler network not prone to over-fitting, like a linear regression).\\

L2-regularization relies on the assumption that a model with small weights is simpler than a model with large weights. Thus, by penalizing the square values of the weights in the cost function we drive all the weights to smaller values. It becomes too costly for the cost to have large weights! This leads to a smoother model in which the output changes more slowly as the input changes.\\

When adding regularization, we'll have cost function and back prop by adding the regularization term's gradient ($\frac{d}{dW} ( \frac{1}{2}\frac{\lambda}{m}  W^2) = \frac{\lambda}{m} W$).\\


\paragraph{Dropout}

It randomly shuts down some neurons in each iteration from one layer to the next. At each iteration, we shut down (set to zero) each neuron of the previous layer with probability \code{1-keep\_prob} or keep it with probability \code{keep\_prob}. When we shut some neurons down, we actually modify the model. The idea behind dropout is that at each iteration, we train a different model that uses only a subset of the neurons. With dropout, the neurons thus become less sensitive to the activation of one other specific neuron, because that other neuron might be shut down at any time.\\

Implementing Dropout, called inverted dropout, create a matrix \code{Dl} for a given layer \code{l}, where each element corresponds to an activation in layer l of the matrix of activations \code{Al}, each element is random values, if this random values is less than \code{keep\_prob}, then if will be assigned 1, we multiply the two and then divide by the \code{keep\_prob} to have the same original scale. One down side of dropout, it that the cost function becomes not defined.\\

For back-propagation: We previously shut down some neurons during forward propagation, by applying a mask D to \code{A1}. In back-propagation, we will have to shut down the same neurons, by re-applying the same mask D to \code{dA1}.\\

A common mistake when using dropout is to use it both in training and testing. we should use dropout (randomly eliminate nodes) only in training, and in inference we multiply the activations by the \code{1/keep\_prob} factor.\\

Other popularization methods can be used, such as data augmentation, or early stooping.

\subsection*{Mini-batches}

Mini-batch: instead of going through the whole training set before doing gradient descent and updating the parameters, we can choose only a portion of the training set, a mini batch, and for each mini batch we update the parameters, the updates might be noisy but converge much faster. The cost function might not go down with each iteration, given that some mini batches contain harder examples, yielding a greater cost function.\\

One Epoch is one pass through the training set. Given that the size of the mini batch = m;
\begin{itemize}
\item If m = size of the whole dataset, then we're talking about regular gradient descent, which takes a lot of time to converge, but with each iteration the cost goes down.
\item If size of the mini batch = 1, then we're talking about stochastic gradient descent, in SGD, we use only 1 training example before updating the gradients.
\end{itemize}

When the training set is large, SGD can be faster. But the parameters will "oscillate" toward the minimum rather than converge smoothly. But we're not using the speed coming from vectorization, the best choice is in between. Typical mini batches are 32, 64, 128, 256, depending on the size of the dataset and how much examples we can store in GPU/CPU memory.\\

There are two steps to apply Mini-Batch Gradient descent:
\begin{enumerate}
\item Shuffle: Create a shuffled version of the training set (X, Y). Note that the random shuffling is done synchronously between X and Y. Such that after the shuffling the ith column of X is the example corresponding to the ith label in Y.
\item Partition: (X, Y) are divided into mini-batches of size \code{mini\_batch\_size}. Note that the number of training examples is not always divisible by \code{mini\_batch\_size}. So the last mini batch might be smaller.
\end{enumerate}

\subsection*{Hyper-parameter tuning}

The common hyper-parameters are: learning rate, number of layers, number of hidden units, learning rate decay, mini-batch size, to name a few.\\

We can use grid search to find the correct combination of hyper-parameter, but instead of trying all the possible pairs, it is better to sample randomly, allowing us to sample over the space of the hyper-parameter more efficiently.\\

But to sample in efficient manner, sometimes we must perform the sampling over the log scale, for example, if we want to find the correct learning rate between 0.0001 and 1, if we sample randomly, 90\% of the values are between 0.1 and 1, so it is better to sample in the log scale.\\

There is two schools of thoughts, using only one model when we don't have enough computation, each training iteration we can try a new set of parameters (Babysitting one model), the second approach is training many models in parallel each one with a different set of parameters and compare them.




\end{document}
              
